/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

#include "contiki.h"
#include "contiki-lib.h"
#include "contiki-net.h"

#include "sys/etimer.h"
#include "sys/stimer.h"
#include "sys/timer.h"
#include "sys/rtimer.h"

#include "dev/leds.h"

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define DEBUG DEBUG_PRINT
#include "net/ip/uip-debug.h"

#define UIP_IP_BUF   ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])

#define MAX_PAYLOAD_LEN 120

static struct uip_udp_conn *server_conn;
//static struct uip_udp_conn *server_conn2;
uint32_t localUTCTime = 0;

void updateUTCTime(uint32_t rplTime){
    
    localUTCTime = rplTime;
}

int getUTCFromLocalTime(){
    
    return localUTCTime;
}
PROCESS(light_process,"lights");
PROCESS(udp_server_process, "UDP server process");
PROCESS(udp_send_process, "UDP sender process");
AUTOSTART_PROCESSES(&resolv_process,&udp_server_process,&udp_server_process,&light_process);
/*---------------------------------------------------------------------------*/
static void
tcpip_handler(void)
{
  static int seq_id;
  char buf[MAX_PAYLOAD_LEN];

  if(uip_newdata()) {
    ((char *)uip_appdata)[uip_datalen()] = 0;
    PRINTF("Server received: '%s' (RSSI: %d) from ", (char *)uip_appdata, (signed short)packetbuf_attr(PACKETBUF_ATTR_RSSI));
    PRINT6ADDR(&UIP_IP_BUF->srcipaddr);
    PRINTF("\n");
    
    
    uint32_t serverTime;
    memcpy(&serverTime,uip_appdata,4);
    updateUTCTime(serverTime);
    printf("time is: %d\n",(int)serverTime);
    uip_ipaddr_copy(&server_conn->ripaddr, &UIP_IP_BUF->srcipaddr);
    PRINTF("Responding with message: ");
 
    sprintf(buf, "Hello from the server! (%d)\n", ++seq_id);
    sprintf(buf, "My current time is %u\n", getUTCFromLocalTime());
    PRINTF("%s\n\r", buf);
    
    uip_udp_packet_sendto(server_conn, buf, strlen(buf),&server_conn->ripaddr, UIP_HTONS(3001));
    /* Restore server connection to allow data from any node */
    //memset(&server_conn2->ripaddr, 0, sizeof(server_conn2->ripaddr));
    memset(&server_conn->ripaddr, 0, sizeof(server_conn->ripaddr));
  }
}
/*---------------------------------------------------------------------------*/
static void
print_local_addresses(void)
{
  int i;
  uint8_t state;

  PRINTF("Server IPv6 addresses: ");
  for(i = 0; i < UIP_DS6_ADDR_NB; i++) {
    state = uip_ds6_if.addr_list[i].state;
    if(uip_ds6_if.addr_list[i].isused &&
       (state == ADDR_TENTATIVE || state == ADDR_PREFERRED)) {
      PRINT6ADDR(&uip_ds6_if.addr_list[i].ipaddr);
      PRINTF("\n\r");
    }
  }
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(udp_server_process, ev, data)
{
#if UIP_CONF_ROUTER
  uip_ipaddr_t ipaddr;
#endif /* UIP_CONF_ROUTER */

  PROCESS_BEGIN();
  PRINTF("UDP server started\n\r");

#if RESOLV_CONF_SUPPORTS_MDNS
  resolv_set_hostname("contiki-udp-server");
#endif

#if UIP_CONF_ROUTER
  uip_ip6addr(&ipaddr, 0xaaaa, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
  uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);
#endif /* UIP_CONF_ROUTER */

  print_local_addresses();

  //Create UDP socket and bind to port 3000
  server_conn = udp_new(NULL, UIP_HTONS(0), NULL);
  udp_bind(server_conn, UIP_HTONS(3000));
  //server_conn2 = udp_new(NULL, UIP_HTONS(0), NULL);
  
  //udp_bind(server_conn2, UIP_HTONS(3001));

  while(1) {
    PROCESS_YIELD();

	//Wait for tcipip event to occur
    if(ev == tcpip_event) {
      tcpip_handler();
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/ 
PROCESS_THREAD(udp_send_process, ev, data){
    static char buf[MAX_PAYLOAD_LEN];
    static struct etimer et;  
    
    
    PROCESS_BEGIN();
    etimer_set(&et,CLOCK_SECOND*5);
    while(1){
        PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_TIMER);
        
		sprintf(buf, "My current time is %u\n\r", getUTCFromLocalTime());
		PRINTF("%s\n\r", buf);
       
		uip_udp_packet_send(server_conn, buf, strlen(buf));
		/* Restore server connection to allow data from any node */
		memset(&server_conn->ripaddr, 0, sizeof(server_conn->ripaddr));
		etimer_set(&et,CLOCK_SECOND * 5);
   }
   PROCESS_END();

}

// red light
PROCESS_THREAD(light_process, ev, data) {
  	PROCESS_BEGIN();	//Start of thread
	leds_off(LEDS_RED);	 //Turn LED off.
	leds_off(LEDS_GREEN);
	static struct etimer et;
	etimer_set(&et,CLOCK_SECOND/10);
	
	while (1) {
		PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_TIMER);
        	if (getUTCFromLocalTime()%10<=4){    
			leds_on(LEDS_RED);
			leds_off(LEDS_GREEN);
		}else{
			leds_off(LEDS_RED);
			leds_on(LEDS_GREEN);
		}
		etimer_set(&et,CLOCK_SECOND/10);
	}
  	PROCESS_END();		//End of thread
}



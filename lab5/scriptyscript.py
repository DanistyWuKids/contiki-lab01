import numpy as np
from math import log10
x = np.array([log10(1), log10(5), log10(10), log10(25), log10(50), log10(100), log10(200), log10(500), log10(1000), log10(1500)])
y = np.array([-25, -28, -30, -40, -48, -52, -55, -65, -70, -74])
A = np.vstack([x, np.ones(len(x))]).T
m,c = np.linalg.lstsq(A,y)[0]
print m,c
